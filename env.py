import gym
import numpy as np


class GeneralSimpleModel(gym.Env):

    def __init__(self, history, t0, t_step, n_steps, big_f_0, k, n_prev, metric="mse"):
        self.history = history
        self._big_f_0 = big_f_0
        self._k = k

        self._last_x = self.history[:n_prev]
        self._t0 = t0
        self._t_step = t_step
        self._n_steps = n_steps
        self._n_prev = n_prev
        self._i = self._n_prev - 1
        self.t = self._t0 + self._t_step * self._i

        if metric not in ["mse", "mspe"]:
            raise ValueError("metric should be 'mse' or 'mspe'")
        self._metric = metric

        self.observation_space = gym.spaces.Box(low=-200, high=200, shape=(self._n_prev,), dtype=np.float32)
        self.action_space = gym.spaces.Box(low=-2, high=2, shape=(1,), dtype=np.float32)
        # self.action_space = gym.spaces.Box(low=-200, high=200, shape=(1,), dtype=np.float32)

    def step(self, action):

        if not (isinstance(action, list) or isinstance(action, np.ndarray) or isinstance(action, tuple)):
            raise TypeError(f"action type is {type(action)}")

        # step
        self._i += 1
        self.t += self._t_step

        # reward
        x_ = action[0]
        if self._metric == "mse":
            reward = - (x_ - self.history[self._i]) ** 2
        elif self._metric == "mspe":
            reward = - ((x_ - self.history[self._i]) / self.history[self._i]) ** 2

        # done
        done = self._i >= self._n_steps - 1

        # info
        info = {}

        # obs
        if self._n_prev > 1:
            self._last_x = self._last_x[1:] + [x_]
        else:
            self._last_x = x_
        obs = np.array(self._last_x).reshape((-1, 1))

        return obs, reward, done, info

    def reset(self):
        self._i = self._n_prev - 1
        self.t = self._t0 + self._t_step * self._i
        self._last_x = self.history[:self._n_prev]
        return np.array(self._last_x).reshape((-1, 1))


class SimpleModel(gym.Env):

    def __init__(self, history, t0, t_step, n_steps, big_f_0, k, n_prev, metric="mse"):
        self.history = history
        self._big_f_0 = big_f_0
        self._k = k

        self._last_x = self.history[:n_prev]
        self._t0 = t0
        self._t_step = t_step
        self._n_steps = n_steps
        self._n_prev = n_prev
        self._i = self._n_prev - 1
        self.t = self._t0 + self._t_step * self._i

        if metric not in ["mse", "mspe"]:
            raise ValueError("metric should be 'mse' or 'mspe'")
        self._metric = metric

        self.observation_space = gym.spaces.Box(low=-200, high=200, shape=(self._n_prev,), dtype=np.float32)
        self.action_space = gym.spaces.Box(np.array([0.1, -2, -.1]), np.array([+3, +2, +.1]), dtype=np.float32)
        # self.action_space = gym.spaces.Box(low=-10, high=10, shape=(3,), dtype=np.float32)

    def _x_model(self, m, delta, big_omega, t=None):
        """Returns the model position."""
        big_b = self._big_b_model(m, delta, big_omega)
        theta = self._theta_model(m, delta, big_omega)
        if t is None:
            t = self.t
        return big_b * np.cos(big_omega * t - theta)

    def _big_b_model(self, m, delta, big_omega):
        omega_0_sqr = self._k / m
        f_0 = self._big_f_0 / m

        res = (omega_0_sqr ** 2 - big_omega ** 2) ** 2
        res += 4 * delta ** 2 * big_omega ** 2
        res = f_0 / res ** 0.5
        return res

    def _theta_model(self, m, delta, big_omega):
        omega_0_sqr = self._k / m

        res = 2 * delta * big_omega
        res /= omega_0_sqr - big_omega ** 2
        return res

    def step(self, action):

        # step
        self._i += 1
        self.t += self._t_step

        # reward
        m_, delta_, big_omega_ = action[0], action[1], action[2]
        x_ = self._x_model(m_, delta_, big_omega_)
        if self._metric == "mse":
            reward = - (x_ - self.history[self._i]) ** 2
        elif self._metric == "mspe":
            reward = - ((x_ - self.history[self._i]) / self.history[self._i]) ** 2

        # done
        done = self._i >= self._n_steps - 1

        # info
        info = {}

        # obs
        if self._n_prev > 1:
            self._last_x = self._last_x[1:] + [x_]
        else:
            self._last_x = x_
        obs = np.array(self._last_x).reshape((-1, 1))

        return obs, reward, done, info

    def reset(self):
        self._i = self._n_prev - 1
        self.t = self._t0 + self._t_step * self._i
        self._last_x = self.history[:self._n_prev]
        return np.array(self._last_x).reshape((-1, 1))

    def render(self):
        pass


class SimpleModelWide(SimpleModel):

    def __init__(self, history, t0, t_step, n_steps, big_f_0, k, n_prev, metric="mse"):
        SimpleModel.__init__(history, t0, t_step, n_steps, big_f_0, k, n_prev, metric="mse")
        self.action_space = gym.spaces.Box(np.array([0, -100, -100]), np.array([+100, +100, +100]), dtype=np.float32)
