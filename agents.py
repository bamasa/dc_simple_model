import torch
import torch.nn as nn
import torch.nn.functional as F
import math
import numpy as np

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class AgentCE(nn.Module):
    def __init__(self, base_env, h_size=16):
        super(AgentCE, self).__init__()
        # state, hidden layer, action sizes
        self.s_size = base_env.observation_space.shape[0]
        self.h_size = h_size
        self.a_size = base_env.action_space.shape[0]
        self.a_high = base_env.action_space.high
        self.a_low = base_env.action_space.low
        self.a_scale = torch.from_numpy(np.abs(self.a_high - self.a_low) / 2)
        self.a_bias = torch.from_numpy((self.a_high + self.a_low) / 2)
        print(self.a_scale, self.a_bias)
        # define layers
        self.fc1 = nn.Linear(self.s_size, self.h_size)
        self.fc2 = nn.Linear(self.h_size, self.a_size)

    def set_weights(self, weights):
        s_size = self.s_size
        h_size = self.h_size
        a_size = self.a_size
        # separate the weights for each layer
        fc1_end = (s_size * h_size) + h_size
        fc1_W = torch.from_numpy(weights[:s_size * h_size].reshape(s_size, h_size))
        fc1_b = torch.from_numpy(weights[s_size * h_size:fc1_end])
        fc2_W = torch.from_numpy(weights[fc1_end:fc1_end + (h_size * a_size)].reshape(h_size, a_size))
        fc2_b = torch.from_numpy(weights[fc1_end + (h_size * a_size):])
        # set the weights for each layer
        self.fc1.weight.data.copy_(fc1_W.view_as(self.fc1.weight.data))
        self.fc1.bias.data.copy_(fc1_b.view_as(self.fc1.bias.data))
        self.fc2.weight.data.copy_(fc2_W.view_as(self.fc2.weight.data))
        self.fc2.bias.data.copy_(fc2_b.view_as(self.fc2.bias.data))

    def get_weights_dim(self):
        return (self.s_size + 1) * self.h_size + (self.h_size + 1) * self.a_size

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = torch.tanh(self.fc2(x) / 5000)  # TODO
        # return x.cpu().data * self.a_scale + self.a_bias
        return x.cpu().data * self.a_scale + self.a_bias

    def evaluate(self, env, weights, gamma=1.0, max_t=5000):
        self.set_weights(weights)
        episode_return = 0.0
        state = env.reset()
        for t in range(max_t):
            state = torch.from_numpy(state).float().to(device)
            action = self.forward(state)
            state, reward, done, _ = env.step(action.numpy())
            episode_return += reward * math.pow(gamma, t)
            if done:
                break
        return episode_return

    def info(self):
        print('observation space:', self.s_size)
        print('action space:', self.a_size)
