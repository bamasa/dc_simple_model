import numpy as np
from collections import deque
import torch
import pickle


def cem(agent, test_env, create_env, weights_save_path, scores_save_path=None, n_iterations=500, max_t=1000, gamma=0.8,
        save_every=200, pop_size=50, percentile=70, sigma=10, c_pop_size=1.0005, c_sigma=0.9995, c_percentile=1.0001,
        lr=0.5, n_eval=10, return_scores=False, only_improvement=False):
    """Cross-entropy method."""
    scores_deque = deque(maxlen=100)
    scores = {"train_average_scores": [], "test_scores": []}

    best_weight = sigma * np.random.randn(agent.get_weights_dim())
    agent.set_weights(best_weight)

    #     print('Episode 0')
    #     plot_result(test_env, agent)

    best_mean_score = -float("inf")
    test_score_with_best_mean_score = -float("inf")
    best_reward = -float("inf")
    for i_iteration in range(1, n_iterations + 1):
        env = create_env()

        weights_pop = [best_weight + (sigma * np.random.randn(agent.get_weights_dim())) for i in range(int(pop_size))]
        rewards = np.array([agent.evaluate(env, weights, gamma, max_t) for weights in weights_pop])

        thr = np.percentile(rewards, percentile)
        elite_weights = [weights_pop[i] for i, r in enumerate(rewards) if r >= thr - 0.01]
        temp_best_weight = (1 - lr) * best_weight + lr * np.array(elite_weights).mean(axis=0)

        rewards = []
        for _ in range(n_eval):
            env = create_env()
            r = agent.evaluate(env, temp_best_weight, gamma=1.0)
            rewards.append(r)
        reward = np.mean(rewards)

        scores_deque.append(reward)
        scores["train_average_scores"].append(reward)

        test_score = agent.evaluate(test_env, temp_best_weight, gamma=1.0)
        scores["test_scores"].append(test_score)

        if reward > best_reward:
            best_reward = reward
            best_weight = temp_best_weight
        else:
            if not only_improvement:
                best_weight = temp_best_weight

        agent.set_weights(best_weight)

        pop_size *= c_pop_size

        sigma *= c_sigma
        sigma = max(0.0001, sigma)

        percentile *= c_percentile
        percentile = min(100, percentile)

        if i_iteration % save_every == 0 or i_iteration == n_iterations:
            mean_score = np.mean(scores_deque)
            if mean_score > best_mean_score:
                torch.save(agent.state_dict(), weights_save_path)
                best_mean_score = mean_score
                test_score_with_best_mean_score = test_score
            print("""Episode {}\tAverage Score: {:.2f}\tTest Score: {:.2f}
Best Average Score: {:.2f}\tTest Score with Best Average Score: {:.2f}""".format(
                i_iteration, mean_score, test_score, best_mean_score, test_score_with_best_mean_score))
            # plot_result(test_env, agent, title_info="test environment")
            if scores_save_path is not None:
                with open(scores_save_path, 'wb') as f:
                    pickle.dump(scores, f)

    if return_scores:
        return scores
