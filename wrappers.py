import gym
import numpy as np


class ImpactWrapper(gym.ObservationWrapper):

    def __init__(self, env, impact_func, param1, param2):
        super().__init__(env)
        self.impact_func = impact_func
        self.param1 = param1
        self.param2 = param2
        self.observation_space = gym.spaces.Box(low=-100, high=100, shape=(2 * self.env._n_prev + 2,), dtype=np.float32)

    def _get_impact(self, obs):
        o = list(obs.reshape((-1,)))
        impact = [self.impact_func(self.t - self.env._t_step * i) for i in range(self.env._n_prev)]
        o = np.array([self.param1, self.param2] + impact + o)
        return o

    def step(self, action):
        o, r, d, info = self.env.step(action)
        return self._get_impact(o), r, d, info

    def reset(self):
        o = self.env.reset()
        return self._get_impact(o)


# class NormActionWrapper(gym.ActionWrapper):
#     def __init__(self, env):
#         super().__init__(env)
#         self._low = self.env.action_space.low
#         self._high = self.env.action_space.high
#
#     def step(self, action):
#         action = (action * (self._high - self._low) + self._high + self._low) / 2
#         o, r, d, info = self.env.step(action)
#         return o, r, d, info
