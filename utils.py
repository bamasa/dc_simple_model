import numpy as np
from sklearn.metrics import mean_squared_error


# def plot_result(env, model, title_info=""):
#     s = env.reset()
#     d = False
#     points = list(s)[-n_prev:]
#     while not d:
#         state = torch.from_numpy(s).float().to(device)
#         a = list(model(state).numpy())
#         s, r, d, _ = env.step(a)
#         points.append(s[-1])
#
#     plt.plot(points, label="agent")
#     plt.plot(history, label="true")
#     plt.title("CE training" + title_info)
#     plt.xlabel("t")
#     plt.ylabel("x")
#     plt.grid(True)
#     plt.legend()
#     plt.show()

def mspe(history, predictions, n_prev):
    """Mean Squared Percentage Error."""
    history = np.array(history[n_prev:])
    predictions = np.array(predictions[n_prev:])
    result = np.linalg.norm((history - predictions) / history)
    result /= len(history)
    return result


def mse(history, predictions, n_prev):
    """Mean Squared Error."""
    history = np.array(history[n_prev:])
    predictions = np.array(predictions[n_prev:])
    return mean_squared_error(history, predictions)

# TODO: add tests
